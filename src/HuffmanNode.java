
public class HuffmanNode implements Comparable<HuffmanNode>
{
//fields
HuffmanNode left;
HuffmanNode right;
int data;
int count;


	public String toString()
	{
		return ""+data+","+count;
	}

	public HuffmanNode(int data)
	{
		this.data = data;
		this.count = 1;
	}
	
	public HuffmanNode()
	{
		this.count = 1;
	}
	
	public HuffmanNode(HuffmanNode left, HuffmanNode right, char data, int count)
	{
		this.left = left;
		this.right = right;
		this.data = data;
		this.setCount(count);
	}
	
	public HuffmanNode(HuffmanNode left, HuffmanNode right, int count)
	{
		this.left = left;
		this.right = right;
		this.setCount(count);
	}
	
	public HuffmanNode(char data, int count)
	{
		this.left = null;
		this.right = null;
		this.data = data;
		this.setCount(count);
	}

	public void setLeft(HuffmanNode left)
	{
		this.left = left;
	}
	public void setRight(HuffmanNode right)
	{
		this.right=right;
	}

	public int compareTo(HuffmanNode arg0) 
	{
		if (this.getCount() > arg0.getCount())
			return 1;
		else if (arg0.getCount() > this.getCount())
			return -1;
		else
			return 0;
		
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
	
//	public boolean hasChar()
//	{
//		if(this.data != null)
//			return true;
//			
//		return false;
//	}
}
