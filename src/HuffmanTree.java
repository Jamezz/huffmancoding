/**
 * CSE143 - Assignment #8 - Huffman Coding
 * This program takes in a file of plaintext and encodes it using a Huffman algorithm (with binary trees). It can also
 * take in a scanner and decode a file that has been previously encoded.
 * 
 * @author Thomas Helms
 */
import java.util.*;
import java.io.*;
public class HuffmanTree 
{
	//fields
	private int endOfFile;
	private PriorityQueue<HuffmanNode> pq;
	private HuffmanNode root;
	
/** constructor
 * used for generating a code file typically from a tree
 * @param counts Incoming array of counted characters. The array can be as large as 255 (the amt of chars in the Ascii Table)
 */
	public HuffmanTree(int[] counts)
	{
		this.endOfFile = counts.length;	//End of file should be the last "whatever" in the array.
		this.pq = new PriorityQueue<HuffmanNode>();
		
		for (int i = 0; i < counts.length; i++)
		{
			HuffmanNode temp = new HuffmanNode((char)i, counts[i]);
			pq.add(temp);
		}
		HuffmanNode eof = new HuffmanNode((char)endOfFile, 1);
		this.pq.add(eof);
		
		root = rgenerateChildren(pq);
		
		
	}
/**constructor (2)
 * used for constructing a tree back from a code file
 * @param input Incoming scanner of the code file.
 */
	public HuffmanTree(Scanner input)
	{
		root = new HuffmanNode();
		while (input.hasNext())
		{
			int character = Integer.parseInt(input.nextLine());
			String traverse = input.nextLine();		
			
			root = buildTree(root, character, traverse);
			
		}
	}
/** This method is designed to take in a bit and determine where it is located on the already-built binary tree.
 * It will find characters until it locateds the end of file sentinel determined by the 'eof' argument.
 * 
 * @param input Scanner-like object that scans a .short file bit by bit.
 * @param output The PrintStream object that is to print back out the decoded file. Could also be used to output to console.
 * @param eof The integer that represents the character for the end of the file.
 */
	public void decode(BitInputStream input, PrintStream output, int eof)
	{
		boolean isDone = false;
		
		while (!isDone)
		{
			HuffmanNode select = root;
			boolean foundChar = false;
			
			while (foundChar == false)
			{
				foundChar = false;
				int bitTemp = input.readBit();
				
				if (bitTemp == 0)
				{
					select = select.left;
				}
				else if (bitTemp == 1)
				{
					select = select.right;
				}
				
				if (select.data > 0 && select.data != eof)
				{
					output.print((char)select.data);
					foundChar = true;
				}
				else if (select.data == eof)
				{
					output.println("\\EOF");
					isDone = true;
					foundChar = true;	//not really true, but we want out of the loop.
				}
			}				
		}
	}
/**
 * Recursive method called by the second constructor (with the scanner object). This method will recursively build the binary tree --
 * back from the scanned code file.
 * 	
 * @param select Current node that is selected in the recursive method.
 * @param character The character we are trying to place in the tree.
 * @param traversal The current direction map of where we still need to go in the tree. (0 is left, 1 is right)
 * @return
 */
	private HuffmanNode buildTree(HuffmanNode select, int character, String traversal)
	{		
		if (traversal.isEmpty())
		{			
			if (character > this.endOfFile)
				endOfFile = character;
			
			return new HuffmanNode(character);
		}
		else if (select == null)
		{
			select = new HuffmanNode();
		}
		
		if (traversal.charAt(0) == '0')
		{
			select.left = buildTree(select.left, character, traversal.substring(1));
		}
		else if(traversal.charAt(0) == '1')
		{
			select.right = buildTree(select.right, character, traversal.substring(1));
		}
		
		return select;
	}
	/** (Called from constructor 1)
	 * Method that is used to build a binary tree from the character usage array.
	 * @param input PriorityQueue generaed from the first constructor. Contains the characters and the amount of characters.
	 * @return The root node of the completed binary tree.
	 */
	private HuffmanNode rgenerateChildren(PriorityQueue<HuffmanNode> input)
	{		
		while (input.size() > 1)
		{			
			
			HuffmanNode input1 = input.poll();
			HuffmanNode input2 = input.poll();
			
			if (input1.count != 0 && input2.count != 0)
			{
				HuffmanNode generate = new HuffmanNode(input1, input2, input1.getCount()+input2.getCount());
				pq.add(generate);	
			}
			else
			{
				if (input1.count != 0)
					pq.add(input1);
				else if (input2.count != 0)
					pq.add(input2);
			}
						
	
		}
		
		HuffmanNode returned = pq.poll();
		
		return returned;
	}
/**Public method that calls recursiveWrite()
 * 	
 * @param output The PrintStream object passed in that we can write back to.
 */
	public void write(PrintStream output)
	{
		recursiveWrite(root, "", output);
	}
/** Recursive method used to write out the binary tree information to a code file.
 * 	
 * @param select Current node in the recursive method.
 * @param traversal Current roadmap to the character we want.
 * @param output The PrintStream object passed in that we can write back to.
 */
	private void recursiveWrite(HuffmanNode select, String traversal, PrintStream output)
	{
		if (select == null)
			return;
		
		if ((int)select.data > 0)
		{
			output.println(select.data);
			output.println(traversal);
		}
		
		recursiveWrite(select.left, traversal+"0", output);
		recursiveWrite(select.right,traversal+"1", output);
				
	}
	
	
}
